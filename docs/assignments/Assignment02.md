# Assignment 02

## Project

### Deadline:

January 26th 2024


### Format:

PDF


### Delivery:

E-mail


### Details:

#### Introduction

Describe the **context** of your biological problem in detail (1-2) pages. References, references, references. This part *absolutely* must be backed up by authoritative sources.


#### Objectives

What did you set out to do? (not *how*, *what*. Do not come up with a list of tasks, but rather specify your ultimate biological objective here).


#### Methods

Describe your list of tasks, and what you have used to solve it. Add details accordingly. Don't forget to cite the **scientific papers of any software you use** (if available). The detail level has to be sufficient to allow the reader to reproduce your analyses. Keep this in mind while writing this part.


#### Results

Thoroughly describe your obtained results. Don't forget to use tables and figures. Non referenced figures and tables **will seriously hamper your grade**. State only the facts, no interpretation.


#### Discussion

Interpret your results and their significance in light of the context provided in the introduction. This section should also be rich in references. Finish with a conclusion if you feel it is necessary.


#### References

This is the last section of your work. It should be large, and filled with mostly **authoritative sources** (blogs, and websites can be used, but must be a minority. The bulk of the entries should be scientific papers and books).
