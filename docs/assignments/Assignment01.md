# Assignment 01

## Project

### Deadline:

October 31st 2023
Ideally, you have this finished by next week. **If you are delivering the project on the deadline, then you are already behind schedule.**


### Format:

PDF


### Delivery:

E-mail


### Details:

* Your project must have:
  * Brief introduction to the problem - The Biological context!
  * Objectives
  * Main methods
  * Expected results
  * Group members and their tasks
* Specify which parts of the approach will be **automated**!


Don't forget the cover and **references**, of course.
