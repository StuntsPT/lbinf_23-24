### classes[0] = "Presentation"

#### Laboratório de Bioinformática 2023-2024

![Logo EST](presentation_assets/logo-ESTB.png)

Francisco Pina Martins

Bruno Costa


---

## Practical info

* &shy;<!-- .element: class="fragment" -->Schedule: Mondays - 14:00 to 18:00 LBI32, **Room 1.10**
* &shy;<!-- .element: class="fragment" -->Questions? f.pina.martins©estbarreiro.ips.pt or bruno.costa©estbarreiro.ips.pt
* &shy;<!-- .element: class="fragment" -->[Moodle](https://moodle.ips.pt/2324/course/view.php?id=538)

---

## Avaliação

* &shy;<!-- .element: class="fragment" --><font color="green">Contínua</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">20% - Contexto sala de aula:</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">10% - Atitude</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">5% - Participação</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">5% - Postura/interesse</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">10% - Projeto (grupo)</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">30% - Relatório Final</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">15% - Relatório</font>
    * &shy;<!-- .element: class="fragment" --><font color="red">15% - Apresentação</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">40% - Trabalho prático (individual)</font>
  * &shy;<!-- .element: class="fragment" --><font color="orange">Sem notas mínimas</font>

---

## How will this work?

* &shy;<!-- .element: class="fragment" -->You will be assigned a (BIG) project
  * &shy;<!-- .element: class="fragment" -->You will have to implement it
  * &shy;<!-- .element: class="fragment" -->Work as a team
  * &shy;<!-- .element: class="fragment" -->Count on us more like "mentors" rather then "teachers"

|||

## New skills?

* &shy;<!-- .element: class="fragment" -->[Git](https://git-scm.com/)
* &shy;<!-- .element: class="fragment" -->[PEP8](https://www.python.org/dev/peps/pep-0008/)
* &shy;<!-- .element: class="fragment" -->[Continuous integration](https://codeship.com/continuous-integration-essentials) and [Docker](https://docker.com)

---

## Picking a project

* &shy;<!-- .element: class="fragment" -->Remember all you learned up to now
* &shy;<!-- .element: class="fragment" -->Pick something you enjoyed
* &shy;<!-- .element: class="fragment" -->Make it awesome

---

## References

* [Git](https://git-scm.com/)
* [PEP8](https://www.python.org/dev/peps/pep-0008/)
* [Continuous integration](https://codeship.com/continuous-integration-essentials)
* [Docker](https://docker.com)
